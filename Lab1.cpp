#include <iostream>
#include <string>

using namespace std;

int main()
{
string str1 = "hello";
string str2 = str1 + "world";

cout << "str1 = " << str1 << endl; 
cout << "the 4th character is " << str1[3] << endl;
cout << "str1 has " << str1.size() << " characters" <<endl;
}
